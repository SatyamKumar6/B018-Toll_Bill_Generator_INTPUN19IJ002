package com.cognizant.TollNaaka.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cognizant.TollNaaka.Model.Toll;
import com.cognizant.TollNaaka.Model.User;
import com.cognizant.TollNaaka.Service.UserServiceImpl;
import com.cognizant.TollNaaka.Service.adminServiceImpl;

@Controller
public class AdminController {
	@Autowired
	UserServiceImpl userServiceImpl;
	@Autowired 
	adminServiceImpl adminServiceImpl;
	
	@RequestMapping(value="/successadmin", method=RequestMethod.POST)
	public String adminRegistration(@ModelAttribute("admin") User admin, ModelMap model) {
		if(adminServiceImpl.addAdmin(admin)) {
			model.put("status", "Your details are submitted successfully");
			model.addAttribute("loginuser", new User());
			return "login";
		}
		else {
			model.put("status", "User Id is already used");
			model.addAttribute("admin", new User());
			return "AdminRegister";
		}
		
	}
	
	@RequestMapping(value="/AdminRegister",method=RequestMethod.GET)
	public String AdminLoginDisplay(ModelMap model) {
		model.addAttribute("admin", new User());
		return "AdminRegister";
	}
	
	@RequestMapping(value="/addDetails",method=RequestMethod.GET)
	public String DisplayaddDetailsAdmin(ModelMap model)
	{
		model.addAttribute("toll", new Toll());
		return "adddetails";
	}

	@RequestMapping(value="/addDetail1",method=RequestMethod.POST)
	public String addDetailsAdmin(@ModelAttribute("toll") Toll toll,ModelMap model)
	{
		if(adminServiceImpl.addDetails(toll))
		{
			model.put("status","Toll Details added Successfully");
			return "adddetails";
		}
		else{
		model.put("status","Toll Details Unable to add");
		return "adddetails";}
	}
	
	@RequestMapping(value="/modifyDetails",method=RequestMethod.GET)
	public String DisplaymodifyDetailsAdmin(ModelMap model)
	{
		model.addAttribute("toll", new Toll());
		return "modifydetails";
	}
	
	@RequestMapping(value="/modifyDetails1",method=RequestMethod.POST)
	public String modifyDetailsAdmin(@ModelAttribute("toll") Toll toll,ModelMap model)
	{
		if(adminServiceImpl.modifyDetails(toll))
		{
			model.put("status","Toll Details Updated Successfully");
			return "modifydetails";
		}
		
		else{
		model.put("status","Toll Details Unable to Update");
		return "modifydetails";
		}
	}
	@RequestMapping(value = "/transitReport", method = RequestMethod.GET)
	public String DisplayTransitDetailsAdmin(ModelMap model) {
		
		List<Map<String, Object>> list = adminServiceImpl.getTollDetail();
		model.addAttribute("TollList", list);
		List<Map<String, Object>> list2 = adminServiceImpl.getAdminDetail();
		model.addAttribute("AdminList", list2);

		return "TransitReport";
	}

	@RequestMapping(value = "/feedbackReport", method = RequestMethod.GET)
	public String DisplayFeedbackDetailsAdmin(ModelMap model) {

		List<Map<String, Object>> list = adminServiceImpl.getFeedbackDetail();
		model.addAttribute("FeedbackList", list);
		return "FeedbackReport";
	}

}

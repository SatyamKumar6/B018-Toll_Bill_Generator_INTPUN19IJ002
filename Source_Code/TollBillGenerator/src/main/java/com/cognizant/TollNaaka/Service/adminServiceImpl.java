package com.cognizant.TollNaaka.Service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cognizant.TollNaaka.Model.Toll;
import com.cognizant.TollNaaka.Model.User;

@Service("adminServiceImpl")
public class adminServiceImpl implements adminService{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public boolean addAdmin(User admin) {

        String sql = "insert into user(role_id, Name, DOB, Mobile_no) values(?,?,?,?)";
        try {
      jdbcTemplate.update(sql,1,admin.getUser_name(),admin.getDateOfBirth(),admin.getMobile_no());
               
        String sql2 = "select max(user_id) from user";
        
        int id =  jdbcTemplate.queryForObject(sql2, Integer.class);
        
        sql2 = "insert into credentials values (?, ?, ?)";
        jdbcTemplate.update(sql2,admin.getEmail_id(),id,admin.getPassword());
        
        
        } catch (Exception e) {
               String sql2 = "select max(user_id) from user";
               int id = jdbcTemplate.queryForObject(sql2, Integer.class);
               String rollback = "delete from user where user_id = ?";
               jdbcTemplate.update(rollback, id);
               
               return false;
        }
        return true;
 }

	@Override
	public boolean addDetails(Toll toll) {
	try{	
	String sql="insert into cities(City_Name) values(?)";
	jdbcTemplate.update(sql,toll.getCityName());
	sql = "Select max(city_id) from cities";
	int city_id = jdbcTemplate.queryForObject(sql, Integer.class);
	sql = "insert into toll (toll_name, toll_level, city_id) values (?,?,?)";
	jdbcTemplate.update(sql, toll.getTollName(),toll.getToll_level(),city_id);
	
	}
	catch(Exception e){
		return false;
	}
		return true;
	}

	@Override
	public boolean modifyDetails(Toll toll) {
		try{
		String sql="update toll set toll_level = ? where toll_name = ?";
		if(jdbcTemplate.update(sql,toll.getToll_level(),toll.getTollName())==1)
		{
			return true;
		}
		else
			return false;
		}
		catch(Exception e){
			return false;
		}
	}
	
	@Override
	public List<Map<String, Object>> getTollDetail() {
		String sql = "select toll_id, toll_name, city_name, toll_level from toll inner join cities on toll.city_id = cities.city_id";
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);
		return dblist;
	}
	
	@Override
	public List<Map<String, Object>> getAdminDetail() {
		String sql = "select  name, DOB, Mobile_no, Email_id from user inner join credentials on user.user_id = credentials.user_id where role_id = 1";
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);
		return dblist;
	}
	
	@Override
	public List<Map<String, Object>> getFeedbackDetail() {
		String sql = "select journey_id, feedback_rating, reviews from feedback";
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);
		return dblist;
	}


}

package com.cognizant.TollNaaka;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.cognizant.TollNaaka.Config.DBConfig;
import com.cognizant.TollNaaka.Service.UserServiceImpl;



@SpringBootApplication(scanBasePackages={"com.cognizant.TollNaaka.Config","com.cognizant.TollNaaka.Model","com.cognizant.TollNaaka.Service","com.cognizant.TollNaaka.Controller"})
//@ComponentScan(basePackages={"com.Tailoring.store.management.Controller","com.Tailoring.store.management.Model","com.Tailoring.store.management.Service","com.Tailoring.store.management.Config"})
public class TollNaaka {
	public static void main(String[] args) {
		SpringApplication.run(TollNaaka.class, args);
		System.out.println("working");
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DBConfig.class);
	}
}

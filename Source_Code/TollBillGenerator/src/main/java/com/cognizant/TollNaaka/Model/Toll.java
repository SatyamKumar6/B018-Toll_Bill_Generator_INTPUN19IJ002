package com.cognizant.TollNaaka.Model;

public class Toll {

	private String cityName;
	private String tollName;
	private float toll_level;
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getTollName() {
		return tollName;
	}
	public void setTollName(String tollName) {
		this.tollName = tollName;
	}
	public float getToll_level() {
		return toll_level;
	}
	public void setToll_level(float toll_level) {
		this.toll_level = toll_level;
	}
	
}

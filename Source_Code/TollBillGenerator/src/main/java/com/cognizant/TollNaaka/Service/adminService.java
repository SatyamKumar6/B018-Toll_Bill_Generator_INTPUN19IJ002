package com.cognizant.TollNaaka.Service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cognizant.TollNaaka.Model.Toll;
import com.cognizant.TollNaaka.Model.User;

@Service
public interface adminService {
		public boolean addAdmin(User admin);
		public boolean addDetails(Toll toll);
		public boolean modifyDetails(Toll toll);
		List<Map<String, Object>> getTollDetail();
		List<Map<String, Object>> getAdminDetail();
		List<Map<String, Object>> getFeedbackDetail();

}

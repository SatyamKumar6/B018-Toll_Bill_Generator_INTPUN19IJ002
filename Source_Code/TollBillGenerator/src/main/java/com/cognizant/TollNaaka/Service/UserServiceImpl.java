package com.cognizant.TollNaaka.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.TollNaaka.Model.DeleteBean;
import com.cognizant.TollNaaka.Model.JourneyDetails;
import com.cognizant.TollNaaka.Model.LoginUser;
import com.cognizant.TollNaaka.Model.ModifyBean;
import com.cognizant.TollNaaka.Model.Rate;
import com.cognizant.TollNaaka.Model.User;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean addUser(User user) {

		String sql = "insert into user(role_id, Name, DOB, Mobile_no) values(?,?,?,?)";
		try {
			jdbcTemplate.update(sql, 2, user.getUser_name(), user.getDateOfBirth(), user.getMobile_no());

			String sql2 = "select max(user_id) from user";

			int id = jdbcTemplate.queryForObject(sql2, Integer.class);

			sql2 = "insert into credentials values (?, ?, ?)";
			jdbcTemplate.update(sql2, user.getEmail_id(), id, user.getPassword());

		} catch (Exception e) {
			String sql2 = "select max(user_id) from user";
			int id = jdbcTemplate.queryForObject(sql2, Integer.class);
			String rollback = "delete from user where user_id = ?";
			jdbcTemplate.update(rollback, id);

			return false;
		}
		return true;
	}

	@Override
	public String isUser(LoginUser user) {
		// TODO Auto-generated method stub
		String userEmail = user.getEmail_id();
		String userpass = user.getPassword();
		String sql = "select email_id, password, role_id from user inner join credentials on user.user_id = credentials.user_id";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		if (user != null) {

			for (Map<String, Object> itUser : list) {

				for (Iterator<Map.Entry<String, Object>> it = itUser.entrySet().iterator(); it.hasNext();) {
					Map.Entry<String, Object> entry = it.next();
					Object value = entry.getValue();
					if (value.equals(userEmail)) {
						entry = it.next();
						Object pass = entry.getValue();
						if (pass.equals(userpass)) {
							entry = it.next();
							Object id = entry.getValue();
							if (id.equals(2)) {
								return "user";
							} else if (id.equals(1)) {
								return "admin";
							}
						} else {

							return "pass";
						}
					}

				}

			}
		}

		return "invalid";
	}

	@Override
	public List<String> getSourceLocations() {
		List<String> list = new ArrayList<>();
		String sql = "select city_name from cities";
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);
		for (Map<String, Object> itUser : dblist) {
			for (Iterator<Map.Entry<String, Object>> it = itUser.entrySet().iterator(); it.hasNext();) {
				Map.Entry<String, Object> entry = it.next();
				String value = (String) entry.getValue();
				list.add(value);
			}
		}
		return list;
	}

	@Override
	public List<String> getVehicleType() {
		List<String> list = new ArrayList<>();
		String sql = "select vehicle_type from vehicle_category";
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);
		for (Map<String, Object> itUser : dblist) {
			for (Iterator<Map.Entry<String, Object>> it = itUser.entrySet().iterator(); it.hasNext();) {
				Map.Entry<String, Object> entry = it.next();
				String value = (String) entry.getValue();
				list.add(value);
			}
		}
		return list;
	}

	@Override
	public float calculateAmount(JourneyDetails journey) {
		String tollPriceSql = "select toll_price_threshold from vehicle_category where vehicle_type ='"
				+ journey.getVehicleType() + "'";
		String query = "select city_id from cities where city_name ='" + journey.getSource() + "'";
		int city_id = jdbcTemplate.queryForObject(query, Integer.class);
		query = "select toll_level from toll where city_id = " + city_id;

		float tollPrice = jdbcTemplate.queryForObject(tollPriceSql, Float.class);
		float tollLevel = jdbcTemplate.queryForObject(query, Float.class);

		if (journey.getDirection().equalsIgnoreCase("return"))
			return (float) (tollLevel * tollPrice * 1.7);

		return (float) tollLevel * tollPrice;

	}

	@Override
	public int putData(JourneyDetails journeyDetail, String username) {

		String sourceCity = "select city_id from cities where city_name = '" + journeyDetail.getSource() + "'";
		String DestCity = "select city_id from cities where city_name = '" + journeyDetail.getDest() + "'";
		String userid = "select user_id from credentials where email_id = '" + username + "'";
		int id = 0;
		int src = jdbcTemplate.queryForObject(sourceCity, Integer.class);
		int dest = jdbcTemplate.queryForObject(DestCity, Integer.class);
		int user = jdbcTemplate.queryForObject(userid, Integer.class);

		String in = "insert into Transition_Details(from_city,to_city,direction,payment_date,journey_date,toll_amount,user_id) values(?,?,?,CURRENT_TIMESTAMP(),STR_TO_DATE(?,'%Y-%m-%dT%H:%i'),?,?)";
		jdbcTemplate.update(in, src, dest, journeyDetail.getDirection(), journeyDetail.getJourneyDate(),
				calculateAmount(journeyDetail), user);
		try {
			in = "select Journey_Id from Transition_Details where User_Id=" + user + " and from_city=" + src
					+ " and to_city=" + dest;
			id = jdbcTemplate.queryForObject(in, Integer.class);

		} catch (Exception e) {
			return 0;
		}
		return id;
	}

	@Override
	public List<Map<String, Object>> getTransitionDetail(String username) {

		String userid = "select user_id from credentials where email_id = '" + username + "'";
		int user = jdbcTemplate.queryForObject(userid, Integer.class);
		String sql = "select journey_id, src_city, C.city_name as dest_city, direction, payment_date, journey_date, toll_amount from cities as C inner join (select journey_id,  to_city, direction, payment_date, journey_date, toll_amount, user_id, B.city_name as src_city from transition_details as A inner join cities as B on B.city_id = A.from_city) as D on C.city_id = D.To_city where user_id = "
				+ user;
		List<Map<String, Object>> dblist = jdbcTemplate.queryForList(sql);

		return dblist;
	}

	@Override
	public float modifyPlan(int toedit, JourneyDetails journeyDetail) {

		String getSql = "select toll_amount from transition_details where journey_id=" + toedit;
		float amount = jdbcTemplate.queryForObject(getSql, Float.class);

		float updateAmt = calculateAmount(journeyDetail);

		String sourceCity = "select city_id from cities where city_name = '" + journeyDetail.getSource() + "'";
		String DestCity = "select city_id from cities where city_name = '" + journeyDetail.getDest() + "'";
		int src = jdbcTemplate.queryForObject(sourceCity, Integer.class);
		int dest = jdbcTemplate.queryForObject(DestCity, Integer.class);
		String sql = "update transition_details set from_city = ?, to_city = ?, direction = ?, journey_date = STR_TO_DATE(?,'%Y-%m-%dT%H:%i'), payment_date = CURRENT_TIMESTAMP(), toll_amount = ? where journey_id = ?";
		jdbcTemplate.update(sql, src, dest, journeyDetail.getDirection(), journeyDetail.getJourneyDate(), updateAmt,
				toedit);

		float toRet = amount - updateAmt;
		return toRet;
	}

	@Override
	public void deletePlan(DeleteBean val) {

		String sql = "delete from transition_details where journey_id=? and journey_date - CURRENT_TIMESTAMP() > 1000000";
		jdbcTemplate.update(sql, val.getJNum());
	}

	@Override
	public void addReview(Rate rate, JourneyDetails journeyDetail, String username) {

		String sourceCity = "select city_id from cities where city_name = '" + journeyDetail.getSource() + "'";
		String DestCity = "select city_id from cities where city_name = '" + journeyDetail.getDest() + "'";
		String userid = "select user_id from credentials where email_id = '" + username + "'";
		int id = 0;
		int src = jdbcTemplate.queryForObject(sourceCity, Integer.class);
		int dest = jdbcTemplate.queryForObject(DestCity, Integer.class);
		int user = jdbcTemplate.queryForObject(userid, Integer.class);
		String in = "select Journey_Id from Transition_Details where User_Id=" + user + " and from_city=" + src
				+ " and to_city=" + dest;
		id = jdbcTemplate.queryForObject(in, Integer.class);
		String sql = "insert into feedback values(?,?,?)";
		jdbcTemplate.update(sql, rate.getRate(), id, rate.getMessage());
	}

	@Override
	public boolean checkJourney(int toedit) {

		String sql = "select count(journey_id) from transition_details where journey_id = " + toedit
				+ " and journey_date - CURRENT_TIMESTAMP() > 1000000";
		int count = jdbcTemplate.queryForObject(sql, Integer.class);
		System.out.println("hvdfjsadfbask:::   " + count);
		if (count == 1) {
			return true;
		}
		return false;
	}

}

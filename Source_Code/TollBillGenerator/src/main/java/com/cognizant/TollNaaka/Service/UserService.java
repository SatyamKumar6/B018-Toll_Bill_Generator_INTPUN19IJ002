package com.cognizant.TollNaaka.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.TollNaaka.Model.DeleteBean;
import com.cognizant.TollNaaka.Model.JourneyDetails;
import com.cognizant.TollNaaka.Model.LoginUser;
import com.cognizant.TollNaaka.Model.ModifyBean;
import com.cognizant.TollNaaka.Model.Rate;
import com.cognizant.TollNaaka.Model.User;

@Service
public interface UserService {
	public boolean addUser(User user);
	public String isUser(LoginUser user);
	public List<String> getSourceLocations();
	public List<String> getVehicleType();
	public float calculateAmount(JourneyDetails journey);
	int putData(JourneyDetails journeyDetail, String username);
	List<Map<String, Object>> getTransitionDetail(String username);
	void deletePlan(DeleteBean val);
	float modifyPlan(int toedit, JourneyDetails journeyDetail);
	void addReview(Rate rate, JourneyDetails journeyDetail, String username);
	boolean checkJourney(int toedit);
}

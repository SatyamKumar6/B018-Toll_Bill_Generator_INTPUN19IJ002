package com.cognizant.TollNaaka.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cognizant.TollNaaka.Model.DeleteBean;
import com.cognizant.TollNaaka.Model.JourneyDetails;
import com.cognizant.TollNaaka.Model.LoginUser;
import com.cognizant.TollNaaka.Model.ModifyBean;
import com.cognizant.TollNaaka.Model.Rate;
import com.cognizant.TollNaaka.Model.User;
import com.cognizant.TollNaaka.Service.UserServiceImpl;

@Controller
public class UserController {

	@Autowired
	UserServiceImpl userServiceImpl;
	
	static JourneyDetails journeyDetail;
	static String username;
	static int toedit;
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String userLoginDisplay(ModelMap model) {
		model.addAttribute("loginuser", new LoginUser());
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute("loginuser") LoginUser user, ModelMap model) {
		String res = userServiceImpl.isUser(user);
		if ("user".equals(res)) {
			model.addAttribute("journey", new JourneyDetails());
			username=user.getEmail_id();
			return "userutility";
		} else if ("admin".equals(res)) {
			model.put("status", "Incorrect Password");
			return "AdminLogin";
		} else if ("pass".equals(res)) {
			model.put("status", "Incorrect Password");
			return "login";
		} else {
			model.put("status", "Not User. Register First");
			return "login";
		}

	}

	@RequestMapping(value = "/UserRegister", method = RequestMethod.GET)
	public String userRegistrationDisplay(ModelMap model) {
		model.addAttribute("user", new User());
		return "UserRegister";
	}


	@RequestMapping(value = "/UserFeedback", method = RequestMethod.GET)
	public String userFeedbackDisplay(ModelMap model) {
		model.addAttribute("rateUs", new Rate());
		return "UserFeedback";
	}
	
	@RequestMapping(value = "/addReview", method = RequestMethod.POST)
	public String userFeedback(@ModelAttribute("rateUs") Rate rate,ModelMap model) {
		model.addAttribute("journey", new JourneyDetails());
	    userServiceImpl.addReview(rate,journeyDetail,username);
		return "userutility";
	}
	
	@RequestMapping(value = "/usersuccess", method = RequestMethod.POST)
	public String userRegistration(@ModelAttribute("user") User user, ModelMap model) {
		if (userServiceImpl.addUser(user)) {
			model.put("status", "Your details are submitted successfully");
			model.addAttribute("loginuser", new LoginUser());

			return "login";
		} else {
			model.put("status", "User Id is already used");
			return "UserRegister";
		}

	}

	@ModelAttribute("sourceList")
	public List<String> sourceListGenerator() {
		List<String> list = userServiceImpl.getSourceLocations();
		return list;
	}

	@ModelAttribute("destList")
	public List<String> destinationListGenerator() {
		List<String> list =userServiceImpl.getSourceLocations();
		return list;
	}

	@ModelAttribute("vehicleTypeList")
	public List<String> vehicleTypeListGenerator() {
		List<String> list =userServiceImpl.getVehicleType();
		return list;
	}

	@ModelAttribute("directionList")
	public List<String> directionListGenerator() {
		List<String> list = new ArrayList<>();
		list.add("Oneway");
		list.add("Return");
		return list;
	}
		
	@RequestMapping(value = "/userutility", method = RequestMethod.POST)
	public String calculate(@ModelAttribute("journey") JourneyDetails journey,ModelMap model) {
		journeyDetail=journey;

		model.put("labelFrom", "From City");
		model.put("fromCity",journeyDetail.getSource());
		model.put("labelTo", "To City");
		model.put("toCity",journeyDetail.getDest());
		model.put("labelDate", "Journey Date");
		model.put("journeyDate",journeyDetail.getJourneyDate());
		model.put("labelNum", "Vehicle Number");
		model.put("vehNum",journeyDetail.getVehicleNum());
		model.put("labelType", "Vehicle Type");
		model.put("vehType", journeyDetail.getVehicleType());
		model.put("labelDirect","Direction");
		model.put("direct",journeyDetail.getDirection());
		model.put("labelPay", "Amount to Pay");
		model.put("amount", userServiceImpl.calculateAmount(journey));
		model.put("pay","<a href=\"payment\"><input type=\"button\" value=\"Pay\"></input></a>");
		return "userutility";
	}

	@RequestMapping(value="/payment",method= RequestMethod.GET)
	public String payment(ModelMap model) {
		return "payment";
	}
	@RequestMapping(value="/creditcard",method= RequestMethod.GET)
	public String creditcard(ModelMap model) {
		model.put("pay","<a href=\"toPay\"><input type=\"button\" value=\"Pay\"></input></a>");
		return "creditcard";
	}
	@RequestMapping(value="/debitcard",method= RequestMethod.GET)
	public String debitcard(ModelMap model) {
		model.put("pay","<a href=\"toPay\"><input type=\"button\" value=\"Pay\"></input></a>");
		return "debitcard";
	}
	@RequestMapping(value="/upi",method= RequestMethod.GET)
	public String upi(ModelMap model) {
		model.put("pay","<a href=\"toPay\"><input type=\"button\" value=\"Pay\"></input></a>");
		return "upi";
	}
	
	@RequestMapping(value = "/toPay", method = RequestMethod.GET)
	public String success(ModelMap model) {
		model.put("username", username);
		model.put("fromCity",journeyDetail.getSource());
		model.put("toCity",journeyDetail.getDest());
		model.put("journeyDate",journeyDetail.getJourneyDate());
		model.put("vehNum",journeyDetail.getVehicleNum());
		model.put("vehType", journeyDetail.getVehicleType());
		model.put("direct",journeyDetail.getDirection());
		model.put("amount", userServiceImpl.calculateAmount(journeyDetail));
		int id=userServiceImpl.putData(journeyDetail,username);
		if(id>0)
		model.put("jid",id);
		else
		{
			model.addAttribute("journey", new JourneyDetails());
			model.put("status", "Already active Plan");
			return "userutility";
		}
		return "showPay";
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.GET)
	public String userModifyDisplay(ModelMap model) {
		List<Map<String, Object>> list =userServiceImpl.getTransitionDetail(username);
		model.addAttribute("valMod",new ModifyBean());
		model.addAttribute("valDel",new DeleteBean());
		model.addAttribute("TransitionList", list);
		return "modifyPlan";
	}
	
	@RequestMapping(value = "/editJ", method = RequestMethod.POST)
	public String editPlan(@ModelAttribute("valMod") ModifyBean val,ModelMap model) {
		model.addAttribute("valMod",new ModifyBean());
		toedit=Integer.parseInt(val.getJNum());
		boolean changeDet = userServiceImpl.checkJourney(toedit);
		if(changeDet){
			model.addAttribute("journey", new JourneyDetails());
			return "editPlan";
			}
		model.addAttribute("valDel",new DeleteBean());
		model.put("status", "Details can't be modified in last 24 hours");
		return "modifyPlan";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String Editcalculate(@ModelAttribute("journey") JourneyDetails journey,ModelMap model) {

		float Retamount=userServiceImpl.modifyPlan(toedit,journey);
		if(Retamount>0)
		{
			model.put("status", "amount to return"+Retamount);
		}
		if(Retamount< 0)
		{
			model.put("status", "Amount to Pay"+Retamount);
		}
		List<Map<String, Object>> list =userServiceImpl.getTransitionDetail(username);
		model.addAttribute("TransitionList", list);
		model.addAttribute("valMod",new ModifyBean());
		model.addAttribute("valDel",new DeleteBean());
		return "modifyPlan";
	}
	@RequestMapping(value = "/deleteJ", method = RequestMethod.POST)
	public String deletePlan(@ModelAttribute("valDel") DeleteBean val,ModelMap model) {
		model.addAttribute("user", new String());
		List<Map<String, Object>> list =userServiceImpl.getTransitionDetail(username);
		model.addAttribute("TransitionList", list);

		try{
		userServiceImpl.deletePlan(val);
		}catch(Exception e)
		{
			model.addAttribute("valMod",new ModifyBean());
			model.addAttribute("valDel",new DeleteBean());
			model.put("status", "Details can't be modified in last 24 hours");

			return "modifyPlan";	
			}

		model.addAttribute("valMod",new ModifyBean());
		model.addAttribute("valDel",new DeleteBean());
		return "modifyPlan";
	}
	@RequestMapping(value = "/updateProfile", method = RequestMethod.GET)
	public String editPlan2(ModelMap model) {
		return "updateProfile";
	}
	@RequestMapping(value = "/userutility1", method = RequestMethod.POST)
	public String editPlan3(ModelMap model) {
		model.addAttribute("journey", new JourneyDetails());
		return "userutility";
	}
}

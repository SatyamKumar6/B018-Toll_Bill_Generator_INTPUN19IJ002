
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport"
    content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
    crossorigin="anonymous">
<title>View</title>
<style>
.t {
    border-collapse: separate;
    border-spacing: 20px 15px;
}

span {
    display: inline-block;
    width: 900px;
    height: 350px;
    margin: 6px;
    background-color: #e7e7e7;
}

/* .btn {
  color: white;
} */
a {
    color: white;
}

a:hover {
    color: white;
}

body {
    background-image:
        url('../images/view.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: 100% 100%;
}

#mydiv {
    position: fixed;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>

</head>
<body>
    <div id="mydiv">
        <div class="container-fluid">
            <span class="border border-success rounded-lg">
                <center>
                    <h1 class="page-header text-center" style="color: #8E44AD">View
                        Student Details</h1>
                    <form>
                        <table class="t">
                            <tr>
                                <td align="center"><input class="btn btn-info"
                                    type="button" value="Print this page" onClick="window.print()"></td>
                            </tr>
                        </table>
                        <table id="tblData" class="t">
                            <tr>
                                <td><label>Student ID</label></td>
                                <td><label>Name</label></td>
                                <td><label>Date of Birth</label></td>
                                <td><label>Mobile No.</label></td>
                                <td><label>Address</label></td>
                                <td><label>User ID</label></td>
                                <td><label>Fees</label></td>
                                <td><label>Gender</label></td>
                                <td><label>Email ID</label></td>
                            </tr>
                            <tr>
                                <td><form:label>${student.id}</form:label></td>
                                <td><form:label>${student.name}</form:label></td>
                                <td><label>${student.date}</label></td>
                                <td><label>${student.mobile}</label></td>
                                <td><label>${student.address}</label></td>
                                <td><label>${student.user_id}</label></td>
                                <td><label>${student.fees}</label></td>
                                <td><label>${student.gender}</label></td>
                                <td><label>${student.emailId}</label></td>
                            </tr>
                        </table>
                        <table class="t">
                            <tr>
                                <td align="center"><input class="btn btn-success"
                                    type="button" value="Export to Excel Sheet"
                                    onclick="exportTableToExcel('tblData')"></td>
                            </tr>
                        </table>
                    </form>
                </center>
            </span>
        </div>
    </div>
</body>
<script type="text/javascript">
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>
</html>


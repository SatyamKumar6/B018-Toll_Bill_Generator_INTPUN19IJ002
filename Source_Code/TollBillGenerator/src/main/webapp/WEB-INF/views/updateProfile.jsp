<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
			<head>
    <meta charset="utf-8">
    <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
		<%@include file="style.css"%>
td{
padding:10px;
}	 
.login-reg-panel{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
	text-align:center;
    width:70%;
	right:0;left:0;
    margin:auto;
    height:400px;
    background-color: rgba(236, 48, 20, 0.9);
}
.white-panel{
    background-color: rgba(255,255, 255, 1);
    height:500px;
    position:absolute;
    top:200px;
    left:35%;
    width:500px;
    right:calc(50% - 50px);
    transition:.3s ease-in-out;
    z-index:0;
    box-shadow: 0 0 15px 9px #00000096;
    opacity:0.9;
}
.login-reg-panel input[type="radio"]{
    position:relative;
    display:none;
}
.login-reg-panel{
    color:#B8B8B8;
}
.login-reg-panel #label-login, 
.login-reg-panel #label-register{
    border:1px solid #9E9E9E;
    padding:5px 5px;
    width:150px;
    display:block;
    text-align:center;
    border-radius:10px;
    cursor:pointer;
    font-weight: 600;
    font-size: 18px;
}
.login-info-box{
	height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.register-info-box{
    height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.right-log{right:50px !important;}

.login-show, 
.register-show{
    z-index: 1;
    display:none;
    opacity:0.5;
    transition:0.3s ease-in-out;
    color:#242424;
    text-align:left;
    padding:50px;
}
.show-log-panel{
	background-image: url(../images/road.jpg);
    display:block;
    opacity:0.8;
}
.login-show input[type="text"], .login-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:20px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.login-show input[type="button"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
}
.login-show a{
    display:inline-block;
    padding:10px 0;
}

.register-show input[type="text"], .register-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:10px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.register-show input[type="button"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
}
.login-show input[type="submit"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
}
.login-show input[type="reset"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
}
.register-show a{
    display:inline-block;
    padding:10px 0;
}   
    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>

<body style="background-image: url(../images/userutil.jpg)">
		<sf:form action="userutility1" method="post">
		
		<br>
		<h1 align="center" class="glow">Edit Transition Details</h1>
	
			<div align="center">${status}</div>		
		
			<div class="white-panel">
				<div class="login-show" >
					     <h2>Update Profile</h2>
                                  <table>
                                  <tr>
                                   <td>Vehicle Number:</td> 
                                
                                  <td><input type="text" required="required"></td>
                                  </tr>
                                  <tr>
                                  <td>Vehicle Type:</td>
                                
                                  <td> <select>
                                  <option>Bus</option>
                                  <option>Car</option>
                                  <option>Three-Wheelers</option>
                                  <option>Truck</option>
                                  <option>Two-Wheelers</option>
                                  
                                  </select></td>
                                  </tr>

                                  <tr>
                                  <td><input type="submit" value="submit"></td>
                      
                                  <td><input type="reset" value="Reset"></td>
                                  </tr>
                                  </table>
				</div>
			</div> 
			   <ul class="nav">
                     <li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li><li></li><li></li><li></li><li></li><li></li><li><li></li>
                           <li class="alignright"><a href="login">LOGOUT</a></li>
                     </ul>
                     <div style="clear: both;"></div>
		</sf:form>

                  
       <script type="text/javascript">

		$(document).ready(function(){
    		$('.login-info-box').fadeOut();
    		$('.login-show').addClass('show-log-panel');
	});


	$('.login-reg-panel input[type="radio"]').on('change', function() {
    		if($('#log-login-show').is(':checked')) {
        	$('.register-info-box').fadeOut(); 
        	$('.login-info-box').fadeIn();
        
        	$('.white-panel').addClass('right-log');
        	$('.register-show').addClass('show-log-panel');
        	$('.login-show').removeClass('show-log-panel');
        
    		}
    		else if($('#log-reg-show').is(':checked')) {
        		$('.register-info-box').fadeIn();
        		$('.login-info-box').fadeOut();
        
        		$('.white-panel').removeClass('right-log');
        
        		$('.login-show').addClass('show-log-panel');
        		$('.register-show').removeClass('show-log-panel');
    		}
	});
	</script>
		
       
</body>

</html>

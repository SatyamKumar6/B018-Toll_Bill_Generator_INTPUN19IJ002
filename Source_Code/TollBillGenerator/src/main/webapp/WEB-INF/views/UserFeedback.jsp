<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
	<html>
	<head>
    <meta charset="utf-8">
    <title>Feedback</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
		<%@include file="style.css"%>
		 
		 
	
		.txt-center {
			text-align: center;
			font-size-adjust: 1;
		}
		.hide {
			display: none;
		}

		.clear {
			float: none;
			clear: both;
		}

		.rating {
			width: 150px;
			unicode-bidi: inherit;
			direction: rtl;
			text-align: center;
			position: relative;
			overflow: hidden;
			
		}

		.rating > label {
			float: right;
			display: inline;
			padding: 0;
			margin: 0;
			position: relative;
			width: 1.5em;
			height: 1.5em;
			cursor: pointer;
			color: #000;
			font-size-adjust: 50px;
		}

		.rating > label:hover,
		.rating > label:hover ~ label,
		.rating > input.radio-btn:checked ~ label {
			color: transparent;
		}

		.rating > label:hover:before,
		.rating > label:hover ~ label:before,
		.rating > input.radio-btn:checked ~ label:before,
		.rating > input.radio-btn:checked ~ label:before {
			content: "\2605";
			position: absolute;
			left: 0;
			color: #FFD700;
		}

		 
.login-reg-panel{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
	text-align:center;
    width:70%;
	right:0;left:0;
    margin:auto;
    height:400px;
    background-color: rgba(236, 48, 20, 0.9);
}
.white-panel{
    background-color: rgba(255,255, 255, 1);
    height:500px;
    position:absolute;
    top:220px;
    left:30%;
    width:800px;
    right:calc(50% - 50px);
    transition:.3s ease-in-out;
    z-index:0;
    box-shadow: 0 0 15px 9px #00000096;
    opacity:0.85;
}
.login-reg-panel input[type="radio"]{
    position:relative;
     width:100px;
     height:50px;
    display:none;
}
.login-reg-panel{
    color:#B8B8B8;
}
.login-reg-panel #label-login, 
.login-reg-panel #label-register{
    border:1px solid #9E9E9E;
    padding:5px 5px;
    width:150px;
    display:block;
    text-align:center;
    border-radius:10px;
    cursor:pointer;
    font-weight: 600;
    font-size: 18px;
}
.login-info-box{
	height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.register-info-box{
    height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.right-log{right:50px !important;}

.login-show, 
.register-show{
    z-index: 1;
    display:none;
    opacity:0.5;
    transition:0.3s ease-in-out;
    color:#242424;
    text-align:left;
    padding:50px;
}
.show-log-panel{
	background-image: url(../images/road.jpg);
    display:block;
    opacity:0.8;
}
.login-show input[type="text"], .login-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:20px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.login-show radiobutton{
    height: 100%;
    width: 200px;
    display: block;
    margin:20px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.login-show input[type="submit"] {
    max-width: 150px;
    border-radius:10px;
    width: 200px;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 20px;
    float:center;
    cursor:pointer;
} 
.login-show input[type="reset"] {
    max-width: 150px;
    border-radius:10px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 20px;
    float:center;
    cursor:pointer;
}
.login-show a{
    display:inline-block;
    padding:10px 0;
}

    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
	
		<body style="background-image: url(../images/feedback.jpg)">
	
	<sf:form action="/addReview" method="post" modelAttribute="rateUs">
<br>
		<h1 align="center" class="glow">Your Feedback is Valuable to Us....!</h1>
		
		
			
			<div class="white-panel">
				<div class="login-show">
					<h2>Rate Your Experiance</h2>
					<div class="rating"><br>
						<sf:radiobutton path="rate" value="5" class="radio-btn hide" label="5"></sf:radiobutton>
						<sf:radiobutton path="rate"  value="4" class="radio-btn hide" label="4"></sf:radiobutton>
						<sf:radiobutton path="rate"  value="3" class="radio-btn hide" label="3"></sf:radiobutton>
						<sf:radiobutton path="rate" value="2" class="radio-btn hide" label="2"></sf:radiobutton>
						<sf:radiobutton path="rate"  value="1" class="radio-btn hide" label="1"></sf:radiobutton>
						<div class="clear"></div>
					</div><br>
		
					<sf:label path="message"><h4>Comments</h4></sf:label><br>
					<sf:textarea path="message" rows="5" cols="50"></sf:textarea><br><br>
			
					<input type="submit" value="Submit Feedback"></input>
				</div>
			</div>
						 
				
				<p align="center">${status}</p>
				
			</div> 
			
			    <ul class="nav">
                     <li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li><li></li><li></li><li></li><li></li><li></li><li>
                           <li class="alignright"><a href="login">LOGOUT</a></li>
                     </ul>
		</sf:form>
	
	<script type="text/javascript">

		$(document).ready(function(){
    		$('.login-info-box').fadeOut();
    		$('.login-show').addClass('show-log-panel');
	});


	$('.login-reg-panel input[type="radio"]').on('change', function() {
    		if($('#log-login-show').is(':checked')) {
        	$('.register-info-box').fadeOut(); 
        	$('.login-info-box').fadeIn();
        
        	$('.white-panel').addClass('right-log');
        	$('.register-show').addClass('show-log-panel');
        	$('.login-show').removeClass('show-log-panel');
        
    		}
    		else if($('#log-reg-show').is(':checked')) {
        		$('.register-info-box').fadeIn();
        		$('.login-info-box').fadeOut();
        
        		$('.white-panel').removeClass('right-log');
        
        		$('.login-show').addClass('show-log-panel');
        		$('.register-show').removeClass('show-log-panel');
    		}
	});
	
	</script>
	
	</body>

</html>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
       pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<sf:form action="/edit" method="post" modelAttribute="journey">
 <h2>Calculate And Pay</h2>
                                  <table>
                                  <tr>
                                  <td>From:</td>
                                  
                                  <td> <sf:select path="source" id="source" items="${sourceList}" /></td>
                                  </tr>
                                  <tr>
                                  <td>To:</td>
                                  
                                  <td> <sf:select path="dest" id="dest" items="${destList}" /></td>
                                  </tr>
                                  <tr>
                                  <td>Journey Date:&nbsp;&nbsp;</td>
                            
                                  <td> <sf:input type="datetime-local" path="journeyDate" id="journeyDate" required="required" /></td>
                                  </tr>
                                  <tr>
                                  <td>Vehicle Number:</td> 
                                
                                  <td><sf:input path="vehicleNum" required="required"></sf:input></td>
                                  </tr>
                                  <tr>
                                  <td>Vehicle Type:</td>
                                
                                  <td> <sf:select path="vehicleType" id="vehicleType" items="${vehicleTypeList}"  /></td>
                                  </tr>
                                  <tr>
                                  <td>Direction:</td> 
                              
                                  <td><sf:select path="direction" id="direction" items="${directionList}" /></td>
                                  </tr>
                                  <tr>
                                  <td><input type="submit" value="Calculate"></td>
                      
                                  <td><input type="reset" value="Reset"></td>
                                  </tr>
                                  </table>
</sf:form>
</body>
</html>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
       pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
              <%@include file="style.css"%>
td,tr{
padding:5px;
}
.login-reg-panel{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
       text-align:center;
    width:70%;
       right:0;left:0;
    margin:auto;
    height:400px;
    background-color: rgba(236, 48, 20, 0.9);
}
.white-panel{
    background-color: rgba(255,255, 255, 1);
    height:600px;
    position:absolute;
    top:200px;
    left:5%;
    width:700px;
    right:calc(50% - 50px);
    transition:.3s ease-in-out;
    z-index:0;
    box-shadow: 0 0 15px 9px #00000096;
    opacity:0.95;
}

.white-panel1{
    background-color: rgba(255,255, 255, 1);
    height:600px;
    position:absolute;
    top:220px;
    left:55%;
    width:700px;
    right:calc(50% - 50px);
    transition:.3s ease-in-out;
    z-index:0;
    box-shadow: 0 0 15px 9px #00000096;
    opacity:0.85;
}
.login-reg-panel input[type="radio"]{
    position:relative;
    display:none;
}
.login-reg-panel{
    color:#B8B8B8;
}
.login-reg-panel #label-login, 
.login-reg-panel #label-register{
    border:1px solid #9E9E9E;
    padding:5px 5px;
    width:150px;
    display:block;
    text-align:center;
    border-radius:10px;
    cursor:pointer;
    font-weight: 600;
    font-size: 18px;
}
.login-info-box{
       height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0; 
    position:absolute;
    text-align:left;
}
.register-info-box{
    height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.right-log{right:50px !important;}

.login-show, 
.register-show{
    z-index: 1;
    display:none;
    opacity:0.5;
    transition:0.3s ease-in-out;
    color:#242424;
    text-align:left;
    padding:50px;
}
.show-log-panel{
       background-image: url(../images/road.jpg);
    display:block;
    opacity:0.8;
}
.login-show input[type="text"], .login-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:20px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.login-show input[type="button"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
} 
.login-show a{
    display:inline-block;
    padding:10px 0;
}

.register-show input[type="text"], .register-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:10px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.register-show input[type="button"] {
    max-width: 150px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 2px;
    float:right;
    cursor:pointer;
}
.register-show a{
    display:inline-block;
    padding:10px 0;
}

    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>


<body style="background-image: url(../images/userutil.jpg);">
       <h1 align="center"  class="glow">Toll Bill Generator</h1>

       <sf:form action="/userutility" method="post" modelAttribute="journey">
       <br>
                     <div class="white-panel">
                           
                           <div class="login-show" >
                           
                                  <h2>Calculate And Pay</h2>
                                  <table>
                                  <tr>
                                  <td>From:</td>
                                  
                                  <td> <sf:select path="source" id="source" items="${sourceList}" /></td>
                                  </tr>
                                  <tr>
                                  <td>To:</td>
                                  
                                  <td> <sf:select path="dest" id="dest" items="${destList}" /></td>
                                  </tr>
                                  <tr>
                                  <td>Journey Date:&nbsp;&nbsp;</td>
                            
                                  <td> <sf:input type="datetime-local" path="journeyDate" id="journeyDate" required="required" /></td>
                                  </tr>
                                  <tr>
                                  <td>Vehicle Number:</td> 
                                
                                  <td><sf:input path="vehicleNum" required="required"></sf:input></td>
                                  </tr>
                                  <tr>
                                  <td>Vehicle Type:</td>
                                
                                  <td> <sf:select path="vehicleType" id="vehicleType" items="${vehicleTypeList}"  /></td>
                                  </tr>
                                  <tr>
                                  <td>Direction:</td> 
                              
                                  <td><sf:select path="direction" id="direction" items="${directionList}" /></td>
                                  </tr>
                                  <tr>
                                  <td><input type="submit" value="Calculate"></td>
                      
                                  <td><input type="reset" value="Reset"></td>
                                  </tr>
                                  </table>
                           </div>
                           </div>
                           <div class="white-panel1">
                           <table>
                           <tr>
                           <td></td></tr>
                           
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelFrom}</td>
                     <td>&nbsp;&nbsp;&nbsp;${fromCity}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelTo}</td>
                     <td>&nbsp;&nbsp;&nbsp;${toCity}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelDate}</td>
                     <td>&nbsp;&nbsp;&nbsp;${journeyDate}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelNum}</td>
                     <td>&nbsp;&nbsp;&nbsp;${vehNum}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelType}</td>
                     <td>&nbsp;&nbsp;&nbsp;${vehType}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelDirect}</td>
                     <td>&nbsp;&nbsp;&nbsp;${direct}</td>
              </tr>
              <tr>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${labelPay}</td>
                     <td>&nbsp;&nbsp;&nbsp;${amount}</td>
              </tr>
       </table>
       <div>
       		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${pay}
       	</div>
       	<div align="center">${status}</div>               
       </div>
       
            
                     <ul class="nav">
                     <li></li>
                           <li class="alignleft"><a href="modify">Modify Plan</a></li>
                           <li class="alignleft"><a href="updateProfile">Update Profile</a></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                           <li></li><li></li><li></li><li></li><li></li><li></li><li><li></li><li></li><li></li><li></li><li></li><li>
                           <li class="alignright"><a href="login">LOGOUT</a></li>
                     </ul>
                     <div style="clear: both;"></div>
       </sf:form>

       
       
       
              <script type="text/javascript">

              $(document).ready(function(){
              $('.login-info-box').fadeOut();
              $('.login-show').addClass('show-log-panel');
       });


       $('.login-reg-panel input[type="radio"]').on('change', function() {
              if($('#log-login-show').is(':checked')) {
             $('.register-info-box').fadeOut(); 
             $('.login-info-box').fadeIn();
        
             $('.white-panel').addClass('right-log');
             $('.register-show').addClass('show-log-panel');
             $('.login-show').removeClass('show-log-panel');
        
              }
              else if($('#log-reg-show').is(':checked')) {
                    $('.register-info-box').fadeIn();
                    $('.login-info-box').fadeOut();
        
                    $('.white-panel').removeClass('right-log');
        
                    $('.login-show').addClass('show-log-panel');
                    $('.register-show').removeClass('show-log-panel');
              }
       });
       
       </script>
       
</body>

</html>

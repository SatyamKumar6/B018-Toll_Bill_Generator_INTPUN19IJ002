<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
		<%@include file="style.css"%>
		 
.login-reg-panel{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
	text-align:center;
    width:70%;
	right:0;left:0;
    margin:auto;
    height:400px;
    background-color: rgba(236, 48, 20, 0.9);
}
.white-panel{
    background-color: rgba(255,255, 255, 1);
    height:700px;
    position:absolute;
    top:200px; 
    left:30%;
    width:800px;
    right:calc(50% - 50px);
    transition:.3s ease-in-out;
    z-index:0;
    box-shadow: 0 0 15px 9px #00000096;
    opacity:0.85;
}
.login-reg-panel input[type="radio"]{
    position:relative;
    display:none;
}
.login-reg-panel{
    color:#B8B8B8;
}
.login-reg-panel #label-login, 
.login-reg-panel #label-register{
    border:1px solid #9E9E9E;
    padding:5px 5px;
    width:150px;
    display:block;
    text-align:center;
    border-radius:10px;
    cursor:pointer;
    font-weight: 600;
    font-size: 18px;
}
.login-info-box{
	height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.register-info-box{
    height: 100%;
    width:50%;
    padding:0 50px;
    top:20%;
    left:0;
    position:absolute;
    text-align:left;
}
.right-log{right:50px !important;}

.login-show, 
.register-show{
    z-index: 1;
    display:none;
    opacity:0.5;
    transition:0.3s ease-in-out;
    color:#242424;
    text-align:left;
    padding:50px;
}
.show-log-panel{
	background-image: url(../images/road.jpg);
    display:block;
    opacity:0.8;
}
.login-show input[type="text"], .login-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:20px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
}
.login-show input[type="submit"] {
    max-width: 150px;
    border-radius:10px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 20px;
    float:center;
    cursor:pointer;
}
.login-show input[type="reset"] {
    max-width: 150px;
    border-radius:10px;
    width: 100%;
    background: #444444;
    color: #f9f9f9;
    border: none;
    padding: 10px;
    text-transform: uppercase;
    border-radius: 20px;
    float:center;
    cursor:pointer;
}
.login-show a{
    display:inline-block;
    padding:10px 0;
}

.register-show input[type="text"], .register-show input[type="password"]{
    height: 100%;
    width: 100%;
    display: block;
    margin:10px 0;
    padding: 10px;
    border: 1px solid #b5b5b5;
    outline: none;
} 
    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>

<body>
<sf:form>
<br>
		<h1 align="center" class="glow">Toll Bill Generator</h1>
			
			<div class="white-panel">
				<div class="login-show">
					<center>
						<h2>Toll Details</h2>
					</center>
				<table id="tblData">
					<tr>
						<td>Toll Id</td>
						<td>Toll Name</td>
						<td>City Name</td>
						<td>Toll Lambda Value</td>
					</tr>
					<c:forEach var="map" items="${TollList}">
						<tr>
							<c:forEach var="mapEntry" items="${map}">
								<td>${mapEntry.value}</td>
							</c:forEach>
						</tr>
					</c:forEach>
					</table>
					
					<br>
					<center><input class="btn btn-success" type="button" value="Export to Excel Sheet" onclick="exportTableToExcel('tblData')"></center>
					
<br>
				<center>
					<h2>Admin Details</h2>
				</center>
			<table id="tbl2Data">
				<tr>
					<td>Name</td>
					<td>DOB</td>
					<td>Mobile Number</td>
					<td>Email ID</td>
				</tr>
				<c:forEach var="map" items="${AdminList}">
					<tr>
						<c:forEach var="mapEntry" items="${map}">
							<td>${mapEntry.value}</td>
						</c:forEach>
					</tr>
				</c:forEach>
				</table>
				<br>
					<center><input class="btn btn-success" type="button" value="Export to Excel Sheet" onclick="exportTableToExcel('tbl2Data')"></center>
				
				</div>
				
				<p align="center">${status}</p>
				
			</div> 
			
			<ul class="nav">
			<li></li>
  				<li><a href="login">Home</a></li>
			</ul>
			
		</sf:form>


<script type="text/javascript">
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}


$(document).ready(function(){
	$('.login-info-box').fadeOut();
	$('.login-show').addClass('show-log-panel');
});


$('.login-reg-panel input[type="radio"]').on('change', function() {
	if($('#log-login-show').is(':checked')) {
	$('.register-info-box').fadeOut(); 
	$('.login-info-box').fadeIn();

	$('.white-panel').addClass('right-log');
	$('.register-show').addClass('show-log-panel');
	$('.login-show').removeClass('show-log-panel');

	}
	else if($('#log-reg-show').is(':checked')) {
		$('.register-info-box').fadeIn();
		$('.login-info-box').fadeOut();

		$('.white-panel').removeClass('right-log');

		$('.login-show').addClass('show-log-panel');
		$('.register-show').removeClass('show-log-panel');
	}
});

</script>
</body>
</html>
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP DATABASE IF EXISTS tollbillgenerator;

CREATE SCHEMA IF NOT EXISTS `tollbillgenerator` DEFAULT CHARACTER SET utf8 ;
USE `tollbillgenerator` ;



-- -----------------------------------------------------
-- Table `tollbillgenerator`.`cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`cities` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`cities` (
  `City_Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `City_Name` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`City_Id`) ,
  UNIQUE INDEX `City_Id_UNIQUE` (`City_Id` ASC) ,
  UNIQUE INDEX `City_Name_UNIQUE` (`City_Name` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`role` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`role` (
  `Role_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Role_Name` VARCHAR(15) NOT NULL ,
  PRIMARY KEY (`Role_id`) ,
  UNIQUE INDEX `Role_id_UNIQUE` (`Role_id` ASC) ,
  UNIQUE INDEX `Role_Name_UNIQUE` (`Role_Name` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`user` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`user` (
  `User_Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Role_id` INT(11) NOT NULL ,
  `Name` VARCHAR(20) NOT NULL ,
  `DOB` DATE NOT NULL ,
  `Mobile_No` VARCHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`User_Id`) ,
  UNIQUE INDEX `User_Id_UNIQUE` (`User_Id` ASC) ,
  INDEX `fk_User_Role1_idx` (`Role_id` ASC) ,
  CONSTRAINT `fk_User_Role1`
    FOREIGN KEY (`Role_id` )
    REFERENCES `tollbillgenerator`.`role` (`Role_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`credentials`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`credentials` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`credentials` (
  `Email_Id` VARCHAR(45) NOT NULL ,
  `User_Id` INT(11) NOT NULL ,
  `Password` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Email_Id`) ,
  UNIQUE INDEX `Email_Id_UNIQUE` (`Email_Id` ASC) ,
  UNIQUE INDEX `User_Id_UNIQUE` (`User_Id` ASC) ,
  INDEX `fk_Credentials_User_idx` (`User_Id` ASC) ,
  CONSTRAINT `fk_Credentials_User`
    FOREIGN KEY (`User_Id` )
    REFERENCES `tollbillgenerator`.`user` (`User_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`transition_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`transition_details` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`transition_details` (
  `Journey_Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `From_city` INT(11) NOT NULL ,
  `To_city` INT(11) NOT NULL ,
  `Direction` VARCHAR(6) NOT NULL ,
  `Payment_Date` DATETIME NOT NULL ,
  `Journey_Date` DATETIME NOT NULL ,
  `Toll_Amount` FLOAT NOT NULL ,
  `User_Id` INT(11) NOT NULL ,
  PRIMARY KEY (`Journey_Id`) ,
  UNIQUE INDEX `Journey_Id_UNIQUE` (`Journey_Id` ASC) ,
  INDEX `fk_Transition_Details_User1_idx` (`User_Id` ASC) ,
  CONSTRAINT `fk_Transition_Details_User1`
    FOREIGN KEY (`User_Id` )
    REFERENCES `tollbillgenerator`.`user` (`User_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `tollbillgenerator`.`feedback`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`feedback` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`feedback` (
  `Feedback_Rating` INT(11) NOT NULL ,
  `Journey_Id` INT(11) NOT NULL ,
  `Reviews` VARCHAR(500) NULL ,
  PRIMARY KEY (`Journey_Id`) ,
  UNIQUE INDEX `Journey_Id_UNIQUE` (`Journey_Id` ASC) ,
  INDEX `fk_Feedback_Transition_Details1_idx` (`Journey_Id` ASC) ,
  CONSTRAINT `fk_Feedback_Transition_Details1`
    FOREIGN KEY (`Journey_Id` )
    REFERENCES `tollbillgenerator`.`transition_details` (`Journey_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `tollbillgenerator`.`toll`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`toll` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`toll` (
  `Toll_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Toll_Name` VARCHAR(15) NOT NULL ,
  `Toll_Level` FLOAT NOT NULL ,
  `City_Id` INT(11) NOT NULL ,
  PRIMARY KEY (`Toll_id`) ,
  UNIQUE INDEX `Toll_id_UNIQUE` (`Toll_id` ASC) ,
  UNIQUE INDEX `Toll_Name_UNIQUE` (`Toll_Name` ASC) ,
  INDEX `fk_Toll_Cities1_idx` (`City_Id` ASC) ,
  CONSTRAINT `fk_Toll_Cities1`
    FOREIGN KEY (`City_Id` )
    REFERENCES `tollbillgenerator`.`cities` (`City_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`vehicle_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`vehicle_category` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`vehicle_category` (
  `Vehicle_Type_Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Vehicle_Type` VARCHAR(15) NOT NULL ,
  `Toll_Price_Threshold` FLOAT NOT NULL ,
  PRIMARY KEY (`Vehicle_Type_Id`) ,
  UNIQUE INDEX `Vehicle_Type_Id_UNIQUE` (`Vehicle_Type_Id` ASC) ,
  UNIQUE INDEX `Vehicle_Type_UNIQUE` (`Vehicle_Type` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tollbillgenerator`.`vehicle_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tollbillgenerator`.`vehicle_details` ;

CREATE  TABLE IF NOT EXISTS `tollbillgenerator`.`vehicle_details` (
  `Vehicle_Number` VARCHAR(45) NOT NULL ,
  `Vehicle_Type_Id` INT(11) NOT NULL ,
  `Journey_Id` INT(11) NOT NULL ,
  PRIMARY KEY (`Journey_Id`) ,
  UNIQUE INDEX `Journey_Id_UNIQUE` (`Journey_Id` ASC) ,
  INDEX `fk_Vehicle_Details_Vehicle_Category1_idx` (`Vehicle_Type_Id` ASC) ,
  INDEX `fk_Vehicle_Details_Transition_Details1_idx` (`Journey_Id` ASC) ,
  CONSTRAINT `fk_Vehicle_Details_Vehicle_Category1`
    FOREIGN KEY (`Vehicle_Type_Id` )
    REFERENCES `tollbillgenerator`.`vehicle_category` (`Vehicle_Type_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vehicle_Details_Transition_Details1`
    FOREIGN KEY (`Journey_Id` )
    REFERENCES `tollbillgenerator`.`transition_details` (`Journey_Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `tollbillgenerator` ;
insert into role(Role_Name) values ("Admin");
insert into role(Role_Name) values ("User");
insert into user(role_id, Name, DOB, Mobile_no) values (2, "Rupesh Sharma", "1984-05-24" ,9874563215);
insert into credentials values ("suresh@gmail.com", 1, 'Suresh123');
insert into user(role_id, Name, DOB, Mobile_no) values (1, "Satyam Kumar", "1992-08-14" ,9874568574);
insert into credentials values ("satyam@gmail.com", 2, 'Satyam');
insert into cities (city_name) values ("Pune"), ("Ahmedabad"), ("Mumbai"), ("Bangaluru"), ("Nashik"),("Hyderabad"),("Gandhinagar"),("Surat"),("Mysuru"),("Thiruvananthapuram");
insert into toll (toll_name, toll_level, city_id) values ("Pune Toll", 1.2, 1), 
("Ahmedabad Toll",1.1,2), ("Mumbai  Toll",1.5,3), ("Bangaluru  Toll",1.6,4), ("Nashik  Toll",1.1,5),("Hyderabad Toll",1.4,6),
("Gnagar Toll",1.3,7),("Surat Toll",1.4,8),("Mysuru Toll",1,9),("Thiru Toll",1,10);
insert into vehicle_category (vehicle_type, toll_price_threshold) values ("Truck", 350.0),("Bus", 250.0),("Car", 150.0),("Two-wheeler", 10.0),("Three-wheelers", 25.0);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
